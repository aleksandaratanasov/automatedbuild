#include <list.h>
#include "minunit.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int tests_run = 0;
List* list = NULL;

static char* test_list_create()
{
    printf("\nTEST: test_list_create\t\t");
    list = createList(1);
    mu_assert("Error: Failed to create list with a single node", list != NULL);
#ifdef DEBUG
    printf("\n--------------------------");
    debugList(list);
#endif

    printf("OK");
    return 0;
}

static char* test_list_create_mulitple_nodes()
{
    printf("\nTEST: test_list_create_multiple_nodes\t\t");
    List* list = createList(4);
    mu_assert("Error: Failed to create list with multiple nodes", list != NULL);
    mu_assert("Error: Failed to create list with multiple nodes", list->length == 4);
#ifdef DEBUG
    printf("\n--------------------------");
    debugList(list);
#endif

    deleteList(&list);
    mu_assert("Error: Failed to delete list", list == NULL);

    printf("OK");
    return 0;
}

static char* test_list_delete()
{
    printf("\nTEST: test_list_delete\t\t");
    deleteList(&list);
    mu_assert("Error: Failed to delete list", list == NULL);

    printf("OK");
    return 0;
}

static char* test_list_create_from_array()
{
    printf("\nTEST: test_list_create_from_array\t");
    int count = 3;
    int values[] = { 1, 1, 1 };

    List* list = createListFromArray(values, count);
    mu_assert("Error: Failed to create list from array", list != NULL);
    mu_assert("Error: Number of nodes of created list from array is incorrect", list->length == count);
    int idx;
    Node* curr;
    for (idx = 0, curr = list->first; idx < count && curr; ++idx, curr = curr->next)
    {
        mu_assert("Error: Value from array does not correspond to value in list", values[idx] == curr->value);
    }
#ifdef DEBUG
        printf("\n--------------------------");
        debugList(list);
#endif

    deleteList(&list);
    mu_assert("Error: Failed to delete list", list == NULL);

    printf("OK");
    return 0;
}

static char* test_list_append()
{
    printf("\nTEST: test_list_append\t\t");
    list = createList(0);
    mu_assert("Error: Failed to create list", list != NULL);
#ifdef DEBUG
    printf("\n--------------------------");
    debugList(list);
#endif

    Node* node = createNode(0);
    mu_assert("Error: Failed to create node", node != NULL);
    mu_assert("Error: Failed to append node to list", appendNode(&list, node) == true);
    mu_assert("Error: Failed to update length of list", list->length == 1);
#ifdef DEBUG
    printf("\n--------------------------");
    debugList(list);
#endif

    node = createNode(1);
    mu_assert("Error: Failed to create node", node != NULL);
    mu_assert("Error: Failed to append node to list", appendNode(&list, node) == true);
    mu_assert("Error: Failed to update length of list", list->length == 2);
#ifdef DEBUG
    printf("\n--------------------------");
    debugList(list);
#endif

    deleteList(&list);
    mu_assert("Error: Failed to delete list", list == NULL);

    printf("OK");
    return 0;
}

static char* test_list_prepend()
{
    printf("\nTEST: test_list_prepend\t\t");
    list = createList(0);
    mu_assert("Error: Failed to create list", list != NULL);
#ifdef DEBUG
    printf("\n--------------------------");
    debugList(list);
#endif

    Node* node = createNode(0);
    mu_assert("Error: Failed to create node", node != NULL);
    mu_assert("Error: Failed to append node to list", prependNode(&list, node) == true);
    mu_assert("Error: Failed to update length of list", list->length == 1);
    mu_assert("Error: Failed to update length of list", list->last->value == node->value);
#ifdef DEBUG
    printf("\n--------------------------");
    debugList(list);
#endif

    node = createNode(1);
    mu_assert("Error: Failed to create node", node != NULL);
    mu_assert("Error: Failed to append node to list", prependNode(&list, node) == true);
    mu_assert("Error: Failed to update length of list", list->length == 2);
    mu_assert("Error: Failed to update length of list", list->first->value == node->value);
#ifdef DEBUG
    printf("\n--------------------------");
    debugList(list);
#endif

    deleteList(&list);
    mu_assert("Error: Failed to delete list", list == NULL);

    printf("OK");
    return 0;
}

static char* test_list_get_values()
{
    printf("\nTEST: test_list_get_values\t\t");
    int count = 6;
    int values[] = { 1, 2, 3, 1, 4, 5};

    List* list = createListFromArray(values, count);

    List* res = findNodeWithValue(1, list);
    mu_assert("Error: Failed to create list from array", list != NULL);
    mu_assert("Error: Number of nodes of created list from array is incorrect", res->length == 2);
    Node* curr;
    for (curr = res->first; curr; curr = curr->next)
    {
        mu_assert("Error: Value from list is incorrect", curr->value == 1);
    }

#ifdef DEBUG
    printf("\n--------------------------");
    debugList(res);
#endif

    List* res2 = findNodeWithValue(10, list);
    mu_assert("Error: Created list for value even though value does not exist", res2 == NULL);

    deleteList(&list);
    mu_assert("Error: Failed to delete list", list == NULL);
    deleteList(&res);
    mu_assert("Error: Failed to delete list", res == NULL);

    printf("OK");
    return 0;
}

static char* test_list_remove_values()
{
    printf("\nTEST: test_list_remove_with_value\t\t");

    int count = 6;
    int values[] = { 1, 2, 3, 1, 4, 5};

    List* list = createListFromArray(values, count);
#ifdef DEBUG
    printf("\n--------------------------");
    debugList(list);
#endif

    removeWithValue(1, &list);
#ifdef DEBUG
    printf("\n--------------------------");
    debugList(list);
#endif
    List* res = findNodeWithValue(1, list);
    mu_assert("Error: Failed to remove nodes with given value", res == NULL && list->length == 4);

    deleteList(&res);
    mu_assert("Error: Failed to delete list", res == NULL);
    deleteList(&list);
    mu_assert("Error: Failed to delete list", list == NULL);

    printf("OK");
    return 0;
}

static char* all_tests() {
    mu_run_test(test_list_create);
    mu_run_test(test_list_delete);
    mu_run_test(test_list_create_mulitple_nodes);
    mu_run_test(test_list_create_from_array);
    mu_run_test(test_list_append);
    mu_run_test(test_list_prepend);
    mu_run_test(test_list_get_values);
    mu_run_test(test_list_remove_values);
    return 0;
}


int main(int argc, char **argv) {
    char *result = all_tests();
    if (result)
        printf("%s\n", result);
    else
        printf("\nALL TESTS PASSED\n");

    printf("Tests run: %d\n", tests_run);

    return result != 0;
}
