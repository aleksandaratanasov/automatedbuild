#include <node.h>
#include "minunit.h"

#include <stdio.h>

int tests_run = 0;
Node* node = NULL;

static char* test_node_create()
{
    printf("\nTEST: test_node_create\t\t");
    node = createNode(0);
    mu_assert("ERROR: Failed to create new node", node != NULL);
    mu_assert("ERROR: Failed to initialize predecessor reference to NULL", node->previous == NULL);
    mu_assert("ERROR: Failed to initialize successor reference to NULL", node->next == NULL);

#ifdef DEBUG
    printf("\n--------------------------");
    debugNode(node);
#endif

    printf("OK");
    return 0;
}

static char* test_node_delete()
{
    printf("\nTEST: test_node_delete\t\t");
    deleteNode(&node);
    mu_assert("ERROR: Failed to delete node", node == NULL);

    printf("OK");
    return 0;
}

static char* test_node_copy()
{
    printf("\nTEST: test_node_copy\t\t");
    Node* n = createNode(0);
    Node* nP = createNode(-1);
    Node* nN = createNode(1);
    n->previous = nP;
    n->next = nN;

#ifdef DEBUG
    printf("\n--------------------------");
    debugNode(n);
#endif

    Node* nCpy = copyNode(n);
    mu_assert("ERROR: Failed to create new node", nCpy != NULL);
    mu_assert("ERROR: Failed to copy reference to preceeding node", nCpy->previous == nP);
    mu_assert("ERROR: Failed to copy reference to superceeding node", nCpy->next == nN);

#ifdef DEBUG
    printf("\n--------------------------");
    debugNode(nCpy);
#endif

    printf("OK");
    return 0;
}

static char* test_node_create_linked()
{
    printf("\nTEST: test_node_create_linked\t\t");
    Node* nodePrev = createNode(-1);
    Node* nodeNext = createNode(1);
    Node* nodeMid = createNodeWithNeighbours(0, nodePrev, NULL);
    mu_assert("ERROR: Failed to create new node with predecessor", nodeMid != NULL &&
                                                                    nodeMid->previous != NULL &&
                                                                    nodeMid->next == NULL);
#ifdef DEBUG
    printf("\n--------------------------");
    debugNode(nodePrev);
    debugNode(nodeMid);
#endif

    deleteNode(&nodeMid);
    mu_assert("ERROR: Failed to delete node", nodeMid == NULL);

    nodeMid = createNodeWithNeighbours(0, NULL, nodeNext);
    mu_assert("ERROR: Failed to create new node with successor", nodeMid != NULL &&
                                                                    nodeMid->previous == NULL &&
                                                                    nodeMid->next != NULL);
#ifdef DEBUG
    printf("\n--------------------------");
    debugNode(nodeMid);
    debugNode(nodeNext);
#endif

    deleteNode(&nodeMid);
    mu_assert("ERROR: Failed to delete node", nodeMid == NULL);

    nodeMid = createNodeWithNeighbours(0, nodePrev, nodeNext);
    mu_assert("ERROR: Failed to create new node with predecessor and successor", nodeMid != NULL &&
                                                                    nodeMid->previous != NULL &&
                                                                    nodeMid->next != NULL);
#ifdef DEBUG
    printf("\n--------------------------");
    debugNode(nodePrev);
    debugNode(nodeMid);
    debugNode(nodeNext);
#endif

    deleteNode(&nodeMid);
    mu_assert("ERROR: Failed to delete node", nodeMid == NULL);
    deleteNode(&nodePrev);
    mu_assert("ERROR: Failed to delete node", nodePrev == NULL);
    deleteNode(&nodeNext);
    mu_assert("ERROR: Failed to delete node", nodeNext == NULL);

    printf("OK");
    return 0;
}

static char* all_tests() {
    mu_run_test(test_node_create);
    mu_run_test(test_node_delete);
    mu_run_test(test_node_copy);
    mu_run_test(test_node_create_linked);

    return 0;
}


int main(int argc, char **argv) {
    char *result = all_tests();
    if (result)
        printf("%s\n", result);
    else
        printf("\nALL TESTS PASSED\n");

    printf("Tests run: %d\n", tests_run);

    return result != 0;
}
