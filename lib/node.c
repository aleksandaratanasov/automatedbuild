#include "node.h"
#include <stdlib.h>
#include <stdio.h>

#ifdef DEBUG
void debugNode(Node* node)
{
    if (!node)
    {
        printf("Invalid node reference");
        return;
    }

    printf("\nNode"
           "\n  value:\t\t%d"
           "\n  addr (prev):\t0x%x"
           "\n  addr (own):\t0x%x"
           "\n  addr (next):\t0x%x\n",
           node->value,
           node->previous,
           node,
           node->next);
}
#endif

Node* createNode(int value)
{
    Node* node = malloc(sizeof(Node));
    if (!node)
        return NULL;

    node->value = value;
    node->previous = NULL;
    node->next = NULL;

    return node;
}

Node* copyNode(Node* node)
{
    if (!node)
        return NULL;

    Node* nodeCpy = createNode(node->value);
    nodeCpy->previous = node->previous;
    nodeCpy->next = node->next;

    return nodeCpy;
}

Node* createNodeWithNeighbours(int value, Node* predecessor, Node* successor)
{
    Node* node = createNode(value);
    if (!node)
        return NULL;

    // If both predecessor and successor are null references a standard node will be created
    if (predecessor) 
    {
        node->previous = predecessor;
        node->previous->next = node;
    }
    if (successor) 
    {
        node->next = successor;
        node->next->previous = node;
    }

    return node;
}

void deleteNode(Node** node)
{
    if (!*node)
        return;

    // Remove reference of predecessor to current node
    if ((*node)->previous)
        (*node)->previous->next = NULL;

    // Remove reference of successor to current node
    if ((*node)->next)
        (*node)->next->previous = NULL;
    
    free(*node);
    *node = NULL;
}
