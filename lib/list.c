#include "list.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <pthread.h>

#ifdef DEBUG
void debugList(List* list)
{
    if (!list)
    {
        printf("\nInvalid list reference\n");
        return;
    }

    if (!list->length)
    {
        printf("\nList is empty\n");
        return;
    }

    Node* curr = list->first;
    while (curr)
    {
        debugNode(curr);
        curr = curr->next;
    }
}
#endif

List* createList(size_t length)
{
    List* list = malloc(sizeof(List));
    if (!list)
        return NULL;

    // Initialize an empty list
    list->first = NULL;
    list->last = NULL;
    list->length = 0;

    // Return the empty list if the length is 0
    if (!length)
        return list;

    // If not create first node
    list->first = createNode(0);
    Node* curr = list->first;
    Node* prev = NULL;

    // Create and attach other nodes if lenght greater than 1
    int nodeIdx;
    for (nodeIdx = 1; nodeIdx < length; ++nodeIdx)
    {
        prev = curr;
        curr = createNodeWithNeighbours(0, prev, NULL);
        prev->next = curr;
    }

    list->last = curr;
    list->length = length;

    return list;
}

List* copyList(List* list)
{
    if (!list)
        return NULL;

    List* copy = createList(list->length);
    if (!copy)
        return NULL;

    Node* curr = list->first;
    while (curr)
    {
        // Create copy of node (see TODO in node.h on copyNode())
        // Append node to list
        curr = curr->next;
    }

    return NULL;
}

List* createListFromArray(int* values, int valuesCount)
{
    if (!valuesCount)
        return NULL;

    List* list = createList(valuesCount);
    if (!list)
        return NULL;

    int* valuesCurr = values;
    Node* curr;
    for (curr = list->first; curr; ++valuesCurr, curr = curr->next)
    {
        curr->value = *valuesCurr;
    }

    return list;
}

void deleteList(List** list)
{
    if(!*list)
        return;

    Node* curr = (*list)->first;
    while (curr)
    {
        Node* next = curr->next;
        deleteNode(&curr);
        curr = next;
        (*list)->first = curr;
        (*list)->length--;
    }

    (*list)->last = NULL;

    free(*list);
    *list = NULL;
}

bool appendNode(List** list, Node* node)
{
    if (!*list || !node)
        return false;

    if (node->previous || node->next)
        return false;

    // If the list was previously empty
    if (!(*list)->length)
        (*list)->first = (*list)->last = node;
    // Else attach the node at the end and update the references
    else
    {
        Node* last = (*list)->last;
        last->next = node;
        node->previous = last;
        node->next = NULL;
        (*list)->last = node;
    }

    (*list)->length++;

    return true;
}

bool prependNode(List** list, Node* node)
{
    if (!*list || !node)
        return false;

    if (node->previous || node->next)
        return false;

    // If the list was previously empty
    if (!(*list)->length)
        (*list)->first = (*list)->last = node;
    // Else attach the node at the beginning and update the references
    else
    {
        Node* first = (*list)->first;
        first->previous = node;
        node->next = first;
        node->previous = NULL;
        (*list)->first = node;
    }

    (*list)->length++;

    return true;
}

// FIXME Valgrind reports conditional with uninitialized value
List* findNodeWithValue(int value, List* list)
{
   if (!list)
       return NULL;

   List* res = createList(0);
   if (!res)
       return NULL;

   Node* curr = list->first;
   while (curr)
   {
       if (curr->value == value)
       {
           Node* node = createNode(0);
           memcpy(node, curr, sizeof(*node));
           node->previous = node->next = NULL;

           if (!appendNode(&res, node))
           {
               deleteList(&res);
               return NULL;
           }
       }

       curr = curr->next;
   }

   if (!res->length)
   {
       deleteList(&res);
   }

   return res;
}

void removeWithValue(int value, List** list)
{
    if (!*list)
        return;

    Node* curr = (*list)->first;
    while (curr)
    {
        if (curr->value == value)
        {
            // Backup references of neighbours (if any)
            Node* prev = curr->previous;
            Node* next = curr->next;

            // Remove node
            deleteNode(&curr);

            // Change length
            (*list)->length--;

            // Node was between two other nodes
            if (prev && next)
            {
                prev->next = next;
                next->previous = prev;

                curr = next;
            }
            // Node was the first one
            else if (!prev && next)
            {
                next->previous = NULL;
                (*list)->first = next;

                // If one node remaining the last is also the first
                if ((*list)->length == 1)
                    (*list)->last = (*list)->first;

                curr = next;
            }
            // Node was the last one
            else
            {
                // FIXME
                prev->next = NULL;
                (*list)->last = prev;

                // If one node remaining the first is also the last
                if ((*list)->length == 1)
                    (*list)->first = (*list)->last;

                curr = prev;
            }
        }
        else if (curr && curr->next)
        {
            curr = curr->next;
        }
        else
        {
            break;
        }
    }
}

void sort(List** list)
{
    if (!*list)
        return;
}
