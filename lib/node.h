/** @file */
#ifndef NODE_H
#define NODE_H


typedef struct Node
{
    /** @brief The value the node contains */
    int value;
    /** @brief A reference to a previous node. Defaults to null */
    struct Node* previous;
    /** @brief A reference to a next node. Defaults to null */
    struct Node* next;
} Node;

/**
 * @brief Creates a node with a given value
 * @param value
 * @return Reference to a new node or NULL if memory allocation has failed
 */
Node* createNode(int value);

/**
 * @brief copyNode
 * @param node
 * @return
 */
Node* copyNode(Node* node); // TODO Shallow copy of node

/**
 * @brief Creates a node with a given value. Optionally its successor and predecessor can be set with existing nodes
 * @param value
 * @param predecessor If null, no predecessor will be added to the newly created node
 * @param successor If null, no successor will be added to the newly created node
 * @return Reference to a new node attached to a predecessor and successor or NULL if memory allocation has failed
 */
Node* createNodeWithNeighbours(int value, Node* predecessor, Node* successor);

/**
 * Deletes a node if a valid reference is passed
 * @param node A valid reference to a node. If reference to previous or next node is present, these will be set to null.
 *             If null, nothing will happen
 */
void deleteNode(Node** node);

#ifdef DEBUG
/**
 * @brief Print address and value information of a node
 * @param node A valid reference to a node. If null, no information will be printed out
 */
void debugNode(Node* node);
#endif

#endif  // NODE_H
