/** @file */
#ifndef LIST_H
#define LIST_H

#include "node.h"
#include <stddef.h>
#include <stdbool.h>

// TODO Add removal of a selected node
// TODO Add insertion of node between two nodes from a list; check for cyclic behaviour?
// TODO Add sorting (e.g. threaded merge sort)
// TODO Add conversion of list to array

/** @brief The list is a container for @ref Node items */
typedef struct List
{
    /** @brief The number of nodes the list contains */
    size_t length;

    /** @brief The first node of the list. If the list contains only a single node this will be the same as @p last */
    Node* first;
    /** @brief The last node of the list. If the list contains only a single node this will be the same as @p first */
    Node* last;
} List;

/**
 * @brief Creates a list with nodes with default value 0
 * @param length The number of nodes to be added to the newly created list
 * @return Reference to a new list or null if memory allocation has failed
 */
List* createList(size_t length);

/**
 * @brief Create a copy of a list from another list. References will not be transferred
 * @param list A valid reference to a list
 * @return Reference to a new list containing all values from @p list if argument is valid, else null
 */
List* copyList(List* list);

/**
 * @brief Creates a list with nodes from an array of values
 * @param values The values for each node. Number of values must be equal to @p valuesCount
 * @param valuesCount The size of the array and the length of the newly created list. Must be equal to the length of @p values
 * @return Reference to a new list with the given values or null if memory allocation has failed
 */
List* createListFromArray(int* values, int valuesCount);

/**
 * @brief Deletes a list and all of its nodes if a valid reference is passed
 * @param list A valid reference to a list. If null, nothing will happen
 */
void deleteList(List** list);

/**
 * @brief Attach a node to the end of the list. The node successor will automatically set to null
 * @param list A valid reference to a list. If null, nothing will happen
 * @param node A valid reference to a node to attach to the list. If null, nothing will happen
 * @return True, if node was successfully attached to list. If list/node is a null reference or node has successor/predecessor, false is returned
 */
bool appendNode(List** list, Node* node);

/**
 * @brief Attach a node to the beginning of the list. The node predecessor will automatically set to null
 * @param list A valid reference to a list. If null, nothing will happen
 * @param node A valid reference to a node to attach to the list. If null, nothing will happen
 * @return True, if node was successfully attached to list. If list/node is a null reference or node has successor/predecessor, false is returned
 */
bool prependNode(List** list, Node* node);

/**
 * @brief Get a list of copies of nodes with the given value from a list
 * @param value Value to search for
 * @param list A valid reference to a list. If null, nothing will happen
 * @return Reference to a new list with one or multiple nodes that have value @p value, else return null reference
 */
List* findNodeWithValue(int value, List* list);

/**
 * @brief Remove all nodes with the given value
 * @param value The value of the node(s) that will be removed. If value is not found inside @p list, nothing will happen
 * @param list The list where the node(s) with @p value will be removed from
 */
void removeWithValue(int value, List** list);

/**
 * @brief Sorts a list using multithreaded merge sort
 * @param list A valid reference to a list. If null, nothing will happen
 */
void sort(List** list); // TODO Implement quick or merge sort using threads

#ifdef DEBUG
/**
 * @brief Print address and value information of a list and all of its nodes
 * @param list A valid reference to a list. If null, no information will be printed out
 */
void debugList(List* list);
#endif

#endif  // LIST_H
